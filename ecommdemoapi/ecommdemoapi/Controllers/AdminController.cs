﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ecommdemoapi.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Admin")]
    public class AdminController : Controller
    {
        [Route("Test")]
        [HttpGet]
        public IActionResult Test()
        {
            var user = User.Identity.IsAuthenticated;
            return Ok(new { message = "ok"});
        }
    }
}