﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ecommdemoapi.Data;
using ecommdemoapi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ecommdemoapi.Controllers
{
    [AllowAnonymous]
    [Produces("application/json")]
    [Route("api/Auth")]
    public class AuthController : Controller
    {
        //private IConfiguration configuration;
        //private SignInManager<IdentityUser> signInManager { get; set; }
        //private RoleManager<IdentityRole> roleManager { get; set; }

        //public AuthController(IConfiguration configuration, SignInManager<IdentityUser> signInManager, RoleManager<IdentityRole> roleManager)
        //{
        //    this.configuration = configuration;
        //    this.signInManager = signInManager;
        //    this.roleManager = roleManager;
        //}

        private readonly ApplicationDbContext _dbContext;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly IConfiguration _config;
        public AuthController(ApplicationDbContext dbContext,
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
          IPasswordHasher<ApplicationUser> passwordHasher,
          IConfiguration config)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _signInManager = signInManager;
            _passwordHasher = passwordHasher;
            _config = config;
        }

        [HttpGet]
        [Route("Token")]
        public async Task<IActionResult> Token()
        {
            try
            {
                string username = Request.Headers["username"].ToString();
                string password = Request.Headers["password"].ToString();

                var usr = await _userManager.FindByNameAsync(username);
                if (usr != null)
                {
                    // Verify that the password matches the hashed password stored in DB
                    if (_passwordHasher.VerifyHashedPassword(usr, usr.PasswordHash, password) == PasswordVerificationResult.Success)
                    {
                        var claims = new[] {
                          new Claim(JwtRegisteredClaimNames.Sub, usr.UserName),
                          new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };
                        // WARNING: Move this to env variable or other place, should not be in the config!
                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("Tokens:Key").Value));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        var token = new JwtSecurityToken(
                          issuer: _config.GetSection("Tokens:Issuer").Value, // website issuing the token
                          audience: _config.GetSection("Tokens:Audience").Value, // website who is going to accept this token
                          claims: claims,
                          expires: DateTime.UtcNow.AddMinutes(90),
                          signingCredentials: creds
                        );
                        IList<string> roles = await _userManager.GetRolesAsync(usr);
                        return Ok(new
                        {
                            access_token = new JwtSecurityTokenHandler().WriteToken(token), // Generates token!
                            role = roles.First(),
                            expiration = token.ValidTo
                        });
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            return BadRequest("Failed to generate token");

            //try
            //{
            //    string username = Request.Headers["username"].ToString();
            //    string password = Request.Headers["password"].ToString();

            //    var signInResult = await this.signInManager.PasswordSignInAsync(username, password, false, false);
            //    if (signInResult.Succeeded)
            //    {
            //        IdentityUser user = await this.signInManager.UserManager.FindByNameAsync(username);
            //        IList<string> roles = await this.signInManager.UserManager.GetRolesAsync(user);

            //        var tokenString = GenerateJSONWebToken(username, password);
            //        return Ok(new { access_token = tokenString, token_type = "Bearer", role = roles.First() });
            //    }
            //    else
            //        return BadRequest("Invalid username or password!");
            //}
            //catch (Exception ex)
            //{
            //    return BadRequest(ex.GetBaseException().ToString());
            //}
        }

        //private string GenerateJSONWebToken(string username, string password)
        //{
        //    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration["Jwt:Key"]));
        //    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

        //    var claims = new[] {
        //        new Claim(JwtRegisteredClaimNames.Email, username),
        //        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
        //    };

        //    var token = new JwtSecurityToken(
        //        this.configuration["Jwt:Issuer"],
        //        this.configuration["Jwt:Issuer"],
        //        claims,
        //        expires: DateTime.Now.AddMinutes(60),
        //        signingCredentials: credentials
        //        );

        //    return new JwtSecurityTokenHandler().WriteToken(token);
        //}

        //[Authorize]
        //[HttpGet]
        //[Route("GetRole")]
        //public async Task<IActionResult> GetRole()
        //{
        //    try
        //    {
        //        IdentityUser user = await this.signInManager.UserManager.FindByNameAsync(User.Identity.Name);
        //        IList<string> roles = await this.signInManager.UserManager.GetRolesAsync(user);
        //        return Ok(new { role = roles.First() });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.GetBaseException().ToString());
        //    }
        //}
    }
}