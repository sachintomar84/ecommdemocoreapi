﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ecommdemoapi.Data;
using ecommdemoapi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ecommdemoapi.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private SignInManager<IdentityUser> _signInManager { get; set; }
        private IConfiguration _configuration { get; }
        public RoleManager<IdentityRole> _roleManager { get; }

        public AccountController(ApplicationDbContext _dbContext, SignInManager<IdentityUser> _signInManager, IConfiguration _configuration,
            RoleManager<IdentityRole> _roleManager)
        {
            this._signInManager = _signInManager;
            this._configuration = _configuration;
            this._roleManager = _roleManager;
        }
        
        [Route("Login")]
        [HttpGet]
        public IActionResult Login(string ReturnUrl)
        {
            return Ok();
        }

        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterUserModel body)
        {
            if (body.Role.ToLower().Contains("admin"))
                return BadRequest("You are not allow to send admin role");

            try
            {
                if (ModelState.IsValid)
                {
                    if (_signInManager.UserManager.FindByNameAsync(body.UserName).Result == null)
                    {
                        var result = _signInManager.UserManager.CreateAsync(new IdentityUser
                        {
                            UserName = body.UserName,
                            Email = body.Email,
                            PhoneNumber = body.PhoneNumber
                        }, body.Password).Result;

                        if (result.Succeeded)
                        {
                            var user = await _signInManager.UserManager.FindByNameAsync(body.UserName);
                            await _signInManager.UserManager.AddToRoleAsync(user, body.Role);
                            return Ok(result.Succeeded);
                        }
                        else
                        {
                            if (result.Errors.Count() > 0)
                                return BadRequest(result.Errors.FirstOrDefault().Description);
                            return BadRequest("Something went wrong");
                        }
                    }
                    else
                    {
                        return BadRequest("User already exists");
                    }
                }
                else
                {
                    return BadRequest("All fields are required");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.GetBaseException());
            }
        }
    }
}